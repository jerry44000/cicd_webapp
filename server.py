import subprocess
from flask import Flask

app = Flask(__name__)

@app.route('/webhook', methods=['POST'])
def webhook():
    # Arrêtez tous les conteneurs en cours d'exécution
    subprocess.run(["docker", "stop"] + subprocess.check_output(["docker", "ps", "-q"]).decode().split())

    # Supprimez tous les conteneurs arrêtés
    subprocess.run(["docker", "rm"] + subprocess.check_output(["docker", "ps", "-aq"]).decode().split())

    # Supprimez toutes les images Docker liées à shaykube/cicd_webapp:latest
    subprocess.run(["docker", "rmi"] + subprocess.check_output(["docker", "images", "-q"]).decode().split())

    # Tirez la dernière image Docker avec le tag :latest
    subprocess.run(["docker", "pull", "shaykube/cicd_webapp:latest"])

    # Lancez le nouveau conteneur Docker avec le tag :latest
    subprocess.run(["docker", "run", "-d", "-p", "8080:80", "shaykube/cicd_webapp:latest"])

    print("Webhook reçu et conteneur mis à jour.")
    return "Webhook traité", 200

if __name__ == '__main__':
    app.run(debug=True, port=5000)
